package database;

import java.io.Serializable;

public class contacto implements Serializable {
    private int _ID;
    private String nombre;
    private String telefono1;
    private String telefono2;
    private String direccion;
    private String notas;
    private int favorite;
    public contacto() {
        this._ID=0;
        this.nombre="";
        this.telefono1="";
        this.telefono2="";
        this.direccion="";
        this.notas="";
        this.favorite=0;
    }
    public contacto(int _ID, String nombre, String telefono1, String telefono2, String direccion, String notas, int favorite) {
         this._ID=_ID;
         this.nombre=nombre;
         this.telefono1=telefono1;
         this.telefono2=telefono2;
         this.direccion=direccion;
         this.notas=notas;
         this.favorite=favorite;
    }
    public int get_ID() {
        return _ID;
    }
    public void set_ID(int _ID) {
        this._ID=_ID;
    }

    public String setNombre(String string){
        this.nombre=nombre;
        return nombre;
    }
    public String setTelefono1(String string) {
        this.telefono1=telefono1;
        return telefono1;
    }
    public String setTelefono2(String string) {
        this.telefono2=telefono2;
        return telefono2;
    }
    public String setDomicilio(String string) {
        this.direccion=direccion;
        return direccion;
    }
    public String setNotas(String string) {
        this.notas=notas;
        return notas;
    }
    public Integer setFavorito(int anInt) {
        this.favorite=favorite;
        return favorite;
    }
    public String getNombre(){
        return nombre;
    }
    public String getTelefono1() {
        return telefono1;
    }
    public String getTelefono2() {
        return telefono2;
    }
    public String getDomicilio() {
        return direccion;
    }
    public String getNotas() {
        return notas;
    }
    public Integer getFavorito() {
        return favorite;
    }
}
