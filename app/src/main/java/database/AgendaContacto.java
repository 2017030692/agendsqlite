package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class AgendaContacto {
    private Context context;
    private AgendaDbHelper agendaDBHelper;
    private SQLiteDatabase db;
    private String[] columnToRead = new String[]{
            DefinirTabla.contacto._ID,
            DefinirTabla.contacto.COLUMN_NAME_NOMBRE,
            DefinirTabla.contacto.COLUMN_NAME_TELEFONO1,
            DefinirTabla.contacto.COLUMN_NAME_TELEFONO2,
            DefinirTabla.contacto.COLUMN_NAME_DIRECCION,
            DefinirTabla.contacto.COLUMN_NAME_NOTAS,
            DefinirTabla.contacto.COLUMN_NAME_FAVORITE
    };

    public AgendaContacto(Context context) {
        this.context = context;
        this.agendaDBHelper = new AgendaDbHelper(this.context);
    }

    public void openDatabase() {
        db = agendaDBHelper.getWritableDatabase();
    }

    public long insertarContacto(contacto c) {
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.contacto.COLUMN_NAME_NOMBRE, c.getNombre());
        values.put(DefinirTabla.contacto.COLUMN_NAME_TELEFONO1, c.getTelefono1());
        values.put(DefinirTabla.contacto.COLUMN_NAME_TELEFONO2, c.getTelefono2());
        values.put(DefinirTabla.contacto.COLUMN_NAME_DIRECCION, c.getDomicilio());
        values.put(DefinirTabla.contacto.COLUMN_NAME_NOTAS, c.getNotas());
        values.put(DefinirTabla.contacto.COLUMN_NAME_FAVORITE, c.getFavorito());

        return db.insert(DefinirTabla.contacto.TABLE_NAME, null, values);
    }

    public long actualizarContacto(contacto c, long id) {
        ContentValues values = new ContentValues();

        values.put(DefinirTabla.contacto.COLUMN_NAME_NOMBRE,c.getNombre());
        values.put(DefinirTabla.contacto.COLUMN_NAME_TELEFONO1,c.getTelefono1());
        values.put(DefinirTabla.contacto.COLUMN_NAME_TELEFONO2,c.getTelefono2());
        values.put(DefinirTabla.contacto.COLUMN_NAME_DIRECCION,c.getDomicilio());
        values.put(DefinirTabla.contacto.COLUMN_NAME_NOTAS,c.getNotas());
        values.put(DefinirTabla.contacto.COLUMN_NAME_FAVORITE,c.getFavorito());

    String criterio = DefinirTabla.contacto._ID + " = " + id;

        return db.update(DefinirTabla.contacto.TABLE_NAME,values,criterio,null);
    }

    public long eliminarContacto(long id) {
        String criterio = DefinirTabla.contacto._ID + " = " + id;
        return db.delete(DefinirTabla.contacto.TABLE_NAME, criterio, null);
    }
    public contacto leerContacto(Cursor cursor) {
        contacto c = new contacto();
        c.set_ID(cursor.getInt(0));
        c.setNombre(cursor.getString(1));
        c.setTelefono1(cursor.getString(2));
        c.setTelefono2(cursor.getString(3));
        c.setDomicilio(cursor.getString(4));
        c.setNotas(cursor.getString(5));
        c.setFavorito(cursor.getInt(6));

        return c;
    }

    public contacto getContacto(long id) {
        contacto contacto = null;
        SQLiteDatabase db = this.agendaDBHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.contacto.TABLE_NAME, columnToRead,
                DefinirTabla.contacto._ID+ " =? ",
                new String[] {String.valueOf(id)},
                null, null, null);
        if (c.moveToFirst()) {
            contacto = leerContacto(c);
        }
        c.close();
        return contacto;
    }
    public ArrayList<contacto> allContactos() {
        ArrayList<contacto> contactos = new ArrayList();
        Cursor cursor = db.query(DefinirTabla.contacto.TABLE_NAME,
                null, null, null,
                null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            contacto c = leerContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }
    public void cerrar() { agendaDBHelper.close(); }
}

